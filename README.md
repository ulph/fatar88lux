# 88 Key, multiple pedal, high resolution MIDI, with some silly extra features.
## Teensy 3.6 board, fatar keybed.

I needed a better keyboard brain for my fatar keybed gutted from a studiologic 990, as I wished for a couple of more feaures;

* multiple continous pedals (continous sustain for pianoteq, expression pedal for fun, etc)
* release velocity (again, pianoteq)
* different velocity curve - I play like a sledge hammer...


The supplied project is really tailored for my setup, but others my find it useful and given enough interest I'm happy to restructure the code to make it easier to adapt to other physical connections/keyboard sizes etc.


There is also a serial command function (quite hacky) as I wanted to control of my experimental features;

* pitch noise
* pitch droop (pitch bend drops as more keys are pressed)
* pedal cc mapping switches

## Complementary notes / schematics
https://www.dropbox.com/sh/fcup6faimrsxodl/AACAnyBIJ8ptioJgFcMSyncha?dl=0

## References / Credits
https://github.com/ast/keyboard
