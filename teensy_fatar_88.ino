#define PRINT_SERIAL_DEBUG 0
#define MIDI_SEMI_PANIC 0 // should not have to :/

#include <errno.h>

/**
TODO
- fix buggy string parsing
*/

/*
* CONFIGURATION
*/

const unsigned int switch_rest_period_micros = 2000; // how long wait for debouncing
const unsigned int scan_period_micros = 50; // how often increment select pin. too often yeilds false readings

// pedal stuff -- todo, make settings?
const float c_port_alpha_fast = 0.9f;
const float c_port_alpha_slow = 0.99f;
const byte g_lsbits = 4; // can't be less than 3 unless code below is fixed
const float c_sensor_hysteres = 2.0f;
const unsigned int port_send_period = 20*1000; // 20ms 

/*
* SETTINGS
*/

struct Settings_t {
  float vel_t0; // period for hardest strike
  float vel_t1; // period for softest recognisable
  byte vel_pow; // power law for mapping
  byte vel_min; // lowest velocity value to send

  byte portmode; // which set of controls to use for the "ports"; wheels+pedals

  byte midi_channel; // which 

  byte root_key; // which is the lowest key on the keybed

  float pb_noise; // randomize pitchbend
  float pb_droop; // for each key pressed, bend sligthly
  float pb_drp_noise; // for each key pressed, randomize pb more
};

Settings_t g_settings = {
  2200,
  500000,
  7,
  0,

  0,

  1,

  21,

  0.f,
  0.f,
  0.f
};

enum kb_pins_t {

  // break / make pins
  kb_MK0  = 0,
  kb_BR0  = 1,

  kb_MK1  = 2,
  kb_BR1  = 3,

  kb_MK2  = 4,
  kb_BR2  = 5,

  kb_MK3  = 6,
  kb_BR3  = 7,

  kb_MK4  = 8,
  kb_BR4  = 9,

  kb_MK5  = 10,
  kb_BR5  = 11,

  kb_MK6  = 12,
  kb_BR6  = 24,

  kb_MK7  = 25,
  kb_BR7  = 26,

  kb_MK8  = 27,
  kb_BR8  = 28,

  kb_MK9  = 29,
  kb_BR9  = 30,

  kb_MK10  = 31,
  kb_BR10  = 32,

  // select pins
  kb_T0  = 23,
  kb_T1  = 22,
  kb_T2  = 21,
  kb_T3  = 20,
  kb_T4  = 19,
  kb_T5  = 18,
  kb_T6  = 17,
  kb_T7  = 16,

  // analog pedal / wheel pins
  kb_P1  = A22,
  kb_P2  = A19,
  kb_P3  = A20,
  kb_P4  = A21,
  kb_P5  = A1,
  kb_P6  = A0,

  // misc pins
  kb_LED = 13,

  kb_none = -1
};

kb_pins_t kbSwitches[] = {
  kb_MK0,
  kb_BR0,
  kb_MK1,
  kb_BR1,
  kb_MK2,
  kb_BR2,
  kb_MK3,
  kb_BR3,
  kb_MK4,
  kb_BR4,
  kb_MK5,
  kb_BR5,
  kb_MK6,
  kb_BR6,
  kb_MK7,
  kb_BR7,
  kb_MK8,
  kb_BR8,
  kb_MK9,
  kb_BR9,
  kb_MK10,
  kb_BR10
};

const byte c_numSwitches = sizeof(kbSwitches) / sizeof(kb_pins_t);

const byte c_numSwitchPairs = c_numSwitches / 2;

kb_pins_t kbSelect[] = {
  kb_T0,
  kb_T1,
  kb_T2,
  kb_T3,
  kb_T4,
  kb_T5,
  kb_T6,
  kb_T7
};

const byte c_numSelect = sizeof(kbSelect) / sizeof(kb_pins_t);

enum key_sense_state_t {
  key_sense_state_unbroken, // send midi OFF when entering
  key_sense_state_broken,
  key_sense_state_made, // send midi ON when entering
  key_sense_state_unmade
};

static unsigned int g_num_switches_closed = 0;

#if PRINT_SERIAL_DEBUG
void debug_invalid_state(key_sense_state_t state, byte invalid){
  if(invalid){
    Serial.print("INVALID: "); Serial.println(state);
  }
}
#else
#define debug_invalid_state(a, b) {}
#endif

struct FatarKeyState {
  byte note;
  kb_pins_t br_pin;
  kb_pins_t mk_pin;
  key_sense_state_t state;
  unsigned long time;
  int midi_balance; // debug

  byte time_to_velocity(unsigned long time){
    // power curve
    float t = time-g_settings.vel_t0;
    t = t<0?0:t;
    t /= g_settings.vel_t1;
    t = t>1?1:t;
    t = (1.0-t);
    for(byte i=1; i<g_settings.vel_pow; i++){
      t *= t;
    }
    return (byte)(127.0*t + 0.49);
  };

  void key_state_event(int note, bool state, unsigned long time){
    // convert to velocity
    byte vel = time_to_velocity(time);

    // send midi message
    if(state){
#if MIDI_SEMI_PANIC
      usbMIDI.sendNoteOff(note, 0, g_settings.midi_channel);
#endif
      usbMIDI.sendNoteOn(note, vel>g_settings.vel_min?vel:g_settings.vel_min, g_settings.midi_channel); // never send on with 0 as it's silly
    }
    else{
      usbMIDI.sendNoteOff(note, vel, g_settings.midi_channel);
    }

#if PRINT_SERIAL_DEBUG
    midi_balance += state?1:-1;
    Serial.print(midi_balance); Serial.print(" "); Serial.print(note); Serial.print(" "); Serial.print(state?1:0); Serial.print(" "); Serial.println(vel);
#endif
  }

  void setState(key_sense_state_t newState, unsigned long clock){
    state = newState;
    time = clock;
  }

  void update(unsigned long now){
    byte br = digitalRead(br_pin); 
    byte mk = digitalRead(mk_pin); 

    switch(this->state){

      case key_sense_state_unbroken:
      debug_invalid_state(state, mk);
      if(br && ( (now - time) > switch_rest_period_micros) ){
        g_num_switches_closed++;
        setState(key_sense_state_broken, now);
      }
      break;

      case key_sense_state_broken:
      if(mk){
        g_num_switches_closed++;
        key_state_event(note, true, now - time);
        setState(key_sense_state_made, now);
      }
      else if(!br){
        g_num_switches_closed--;
        setState(key_sense_state_unbroken, now);                
      }
      break;

      case key_sense_state_made:
      debug_invalid_state(state, !br);
      if( !mk && ( (now - time) > switch_rest_period_micros) ){
        g_num_switches_closed--;
        setState(key_sense_state_unmade, now);
      }
      break;

      case key_sense_state_unmade:
      if( !br ){
        g_num_switches_closed--;
        key_state_event(note, false, now - time);
        setState(key_sense_state_unbroken, now);
      }
      else if(mk){
        g_num_switches_closed++;
        setState(key_sense_state_made, now);
      }
      break;

      default:
      break;
    }

  }
};

static FatarKeyState g_KeyStates[c_numSwitchPairs*c_numSelect] = {};

float pb_bias_factor = 1.0;
byte pb_pin = (byte)kb_P5; // :/
void calibrate_pb(){
  float centerpoint = analogRead(pb_pin);
  pb_bias_factor = 512.f / centerpoint;
}

enum port_type_t {
  port_type_pb,
  port_type_cc,
  port_type_at,
  port_type_note_single, // TODO - hold note until release (release vel)
  port_type_note_double  // TODO - diff note on down/up, with some hysteris
};

struct PortState {
  kb_pins_t pin;
  port_type_t type;
  float sticky_delta;
  byte cc_msb;
  byte cc_lsb;
  float value_fast;
  float value_slow;
  float effective_value;
  unsigned long last_send_time;
  byte last_sent_msb;
  byte last_sent_lsb;
  byte last_last_sent_msb;
  byte last_last_sent_lsb;

  bool value_should_be_sent(byte msb){
    return !(last_sent_msb == msb);
  }

  bool value_should_be_sent(byte msb, byte lsb){
    bool send = true;
    send = send && !(last_sent_msb == msb && last_sent_lsb == lsb);
    send = send && !(last_last_sent_msb == msb && last_last_sent_lsb == lsb);
    return send;
  }

  void bookkeep_value(byte msb, byte lsb){
      last_last_sent_msb = last_sent_msb;
      last_last_sent_lsb = last_sent_lsb;
      last_sent_msb = msb;
      last_sent_lsb = lsb;    
  }

  void update(unsigned long now){
    float new_value = analogRead(pin);
    byte ls_bits = g_lsbits;

    value_fast = value_fast*c_port_alpha_fast + (1-c_port_alpha_fast)*new_value;
    value_slow = value_slow*c_port_alpha_slow + (1-c_port_alpha_slow)*new_value;

    if(fabs(value_slow-value_fast) < c_sensor_hysteres){
      float sticky_target = value_slow<sticky_delta ? 0 : (value_slow > (1023-sticky_delta) ? 1023 : value_slow);
      effective_value = effective_value*c_port_alpha_slow + (1-c_port_alpha_slow)*sticky_target;
    }else{
      effective_value = value_fast;
    }
    // TODO, split stuff from here on into subclasses
    if((now - last_send_time) >= port_send_period){
      last_send_time = now;
      switch(type){
        case port_type_cc:
        {
          if(cc_lsb){
            unsigned int v = (effective_value * (1<<(ls_bits-3)) + .5);
            byte msb = v >> ls_bits;
            byte lsb = v & ((1<<ls_bits)-1);
            lsb <<= (7-ls_bits);
            lsb |= effective_value>1022.9 ? 0xF:0x0; // saturate up
            if(value_should_be_sent(msb, lsb))
            {
              bookkeep_value(msb, lsb);
              usbMIDI.sendControlChange(cc_msb, msb, g_settings.midi_channel);
              usbMIDI.sendControlChange(cc_lsb, lsb, g_settings.midi_channel);
#if PRINT_SERIAL_DEBUG
              Serial.print(effective_value); Serial.print(" -> cc"); 
              Serial.print(cc_msb); Serial.print(": "); Serial.print(msb); Serial.print(", cc");
              Serial.print(cc_lsb); Serial.print(": "); Serial.println(lsb);
#endif
            }
          }
          else{
            byte msb = ((unsigned int) effective_value) >> 3;
            if(value_should_be_sent(msb))
            {
              bookkeep_value(msb, 0);
              usbMIDI.sendControlChange(cc_msb, msb, g_settings.midi_channel);
#if PRINT_SERIAL_DEBUG
              Serial.print(effective_value); Serial.print(" -> cc"); Serial.print(cc_msb); Serial.print(": "); Serial.println(msb);
#endif
            }
          }
        }
        break;

        case port_type_pb:
        {
          float pb = (pb_bias_factor*effective_value);
          pb = fabs(pb-512)<20?512:pb; // rather large value required unfortunately
          float noise_pow = g_settings.pb_noise + g_settings.pb_drp_noise*g_num_switches_closed;
          pb -= g_settings.pb_droop*g_num_switches_closed;
          pb += random(-noise_pow, noise_pow);
          pb = pb > 0.f ? pb : 0.f;
          pb = pb < 1023.f ? pb:1023.f;
          unsigned int val = ( (pb + 1) * (float)(1<<4) - 0.5);
          byte msb = val >> 7;
          byte lsb = val & ((1<<7)-1);
          if(value_should_be_sent(msb, lsb))
          {
            bookkeep_value(msb, lsb);
            usbMIDI.sendPitchBend(val, g_settings.midi_channel);
#if PRINT_SERIAL_DEBUG
            Serial.print(effective_value); Serial.print(": "); Serial.print(pb); Serial.print(" -> pb: "); Serial.println(val);
#endif
          }
        }
        break;

        case port_type_at:
        {
          unsigned int msb =  ((unsigned int) ( (effective_value + 1)) >> 3);
          msb = msb>127?127:msb;
          if(value_should_be_sent(msb))
          {
            bookkeep_value(msb, 0);
            usbMIDI.sendAfterTouch(msb, g_settings.midi_channel);
#if PRINT_SERIAL_DEBUG
            Serial.print(effective_value); Serial.print(" -> at: "); Serial.println(msb);
#endif
          }
        }
        break;
        
        default:
        break;
      }

    }
  };
};

static PortState g_PortStates_uhe[] = {
  {(kb_pins_t)pb_pin, port_type_pb, 10}, // pitchbend
  {kb_P6, port_type_cc, 25, 1, 33}, // modwheel
  {kb_P1, port_type_cc, 2, 11, 43}, // expression
  {kb_P2, port_type_cc, 2, 2, 34}, // breath
  {kb_P3, port_type_at, 2}, // after-touch
  {kb_P4, port_type_cc, 2, 64, 0} // sustain
};

static PortState g_PortStates_pianoteq[] = {
  {(kb_pins_t)pb_pin,  port_type_pb, 10}, // pitchbend
  {kb_P6, port_type_cc, 25, 1, 33}, // modwheel
  {kb_P1, port_type_cc, 2, 69, 0},
  {kb_P2, port_type_cc, 2, 67, 0},
  {kb_P3, port_type_cc, 2, 66, 0}, 
  {kb_P4, port_type_cc, 2, 64, 0} 
};

const byte c_numPorts = sizeof(g_PortStates_uhe) / sizeof(PortState);

PortState *g_PortStates[] = {
  g_PortStates_uhe,
  g_PortStates_pianoteq
};

const byte c_numPortModes = sizeof(g_PortStates) / sizeof(PortState *);

void setup_keys(){
  for(int g=0; g < c_numSelect; g++){
    for(int i=0; i < c_numSwitchPairs; i++){
      int state_idx = g + 8*i;
      FatarKeyState *keyState = &g_KeyStates[state_idx];
      keyState->note = g_settings.root_key+state_idx;
      keyState->br_pin = kbSwitches[2*i+1];
      keyState->mk_pin = kbSwitches[2*i+0];
    }
  }
}

void setup_pins(){
  int i=0;
  for(i=0; i<c_numSwitches; i++){
    pinMode(kbSwitches[i], INPUT_PULLDOWN);
  }
  for(i=0; i<c_numSelect; i++){
    pinMode(kbSelect[i], OUTPUT);
  }  
  for(i=0; i<c_numPorts; i++){ // TODO, split this out from PortState!
    PortState *portState = &g_PortStates[g_settings.portmode][i];
    pinMode(portState->pin, INPUT);
  }
  
  // debug pins
  pinMode(kb_LED, OUTPUT);
}

void setup(){
  setup_pins();
  setup_keys();
  calibrate_pb();
  Serial.begin(9600);
  usbMIDI.begin();
}

static byte g_select = 0;
void increment_select(){
  digitalWrite(kbSelect[g_select], LOW);  
  g_select++;
  g_select %= c_numSelect;
  digitalWrite(kbSelect[g_select], HIGH);
}

unsigned long last_scan_micros = 0;
unsigned long last_send_usb = 0;
void loop() {

  while(usbMIDI.read()){}
  parse_serial_parse_input();

  unsigned long now = micros();

  if((now - last_scan_micros) < scan_period_micros){
    return;
  }
  last_scan_micros = now;

  for(int i=0; i < c_numSwitchPairs; i++){
    int state_idx = g_select + 8*i;
    FatarKeyState *keyState = &g_KeyStates[state_idx];    
    keyState->update(now);
  }
  increment_select();

  // scan ports and send messages
  for(int i=0; i < c_numPorts; i++){
    PortState *portState = &g_PortStates[g_settings.portmode][i];
    portState->update(now);
  }

  digitalWrite(kb_LED, g_num_switches_closed);

};

struct SerialSetting_t {
  const char * command;
  int(*setter)(float);
  float(*getter)(void);
};

#define mk_setter(what, type, min, max) \
int set_##what(float val ){ \
  if(val < min || val > max){ \
    return -1; \
  } \
  g_settings.what = (type)val; \
  return 0; \
};

#define mk_getter(what) \
float get_##what(){ \
  return g_settings.what ; \
};

#define mk_setandget(what, type, min, max) \
mk_getter(what); \
mk_setter(what, type, min, max);

mk_setandget(vel_t0, float, 2000, 4000);
mk_setandget(vel_t1, float, 5000, 5000000);
mk_setandget(vel_pow, byte, 1, 10);
mk_setandget(vel_min, byte, 0 , 127);
mk_setandget(portmode, byte, 0, c_numPortModes-1);
mk_setandget(midi_channel, byte, 1, 16);
mk_setandget(root_key, byte, 0, 100);
mk_setandget(pb_noise, float, 0, 1000);
mk_setandget(pb_droop, float, -1000, 1000);
mk_setandget(pb_drp_noise, float, 0, 1000);

#define mk_cmd(what) {#what, &set_##what, &get_##what}

SerialSetting_t g_serial_settings[] = {
  mk_cmd(vel_t0),
  mk_cmd(vel_t1),
  mk_cmd(vel_pow),
  mk_cmd(vel_min),
  mk_cmd(portmode),
  mk_cmd(midi_channel),
  mk_cmd(root_key),
  mk_cmd(pb_noise),
  mk_cmd(pb_droop),
  mk_cmd(pb_drp_noise),
  {NULL}
};

void parse_command(const char * serial_msg){
  if(strstr(serial_msg, "set ") == serial_msg){
    byte setting = 0;
    const char * substring = serial_msg + strlen("set ");
    while(g_serial_settings[setting].command){
      if(g_serial_settings[setting].setter){
        if(strstr(substring, g_serial_settings[setting].command) == substring){
          errno = 0;
          float val = strtod(substring+strlen(g_serial_settings[setting].command), NULL);
          if(errno == 0){
            Serial.print("set:\n  ");
            Serial.print(g_serial_settings[setting].command); 
            Serial.print(": "); 
            Serial.print(val);
            Serial.println(g_serial_settings[setting].setter(val) == 0 ? " ok": " not ok");
            return;
          }
          errno = 0;
        }
      }
      setting++;
    }
    Serial.print("set:\n  "); Serial.print(substring); Serial.println(" failed");    
    return;
  }
  else if(strstr(serial_msg, "get ") == serial_msg){
    byte setting = 0;
    const char * substring = serial_msg + strlen("get ");
    while(g_serial_settings[setting].command){
      if(g_serial_settings[setting].getter){
        if(strstr(substring, g_serial_settings[setting].command) == substring){
          Serial.print("get:\n  ");
          Serial.print(g_serial_settings[setting].command); 
          Serial.print(": "); 
          Serial.println(g_serial_settings[setting].getter());
          return;
        }
      }
      setting++;
    }
    Serial.print("get\n  "); Serial.print(substring); Serial.println(" failed");
    return;
  }
  else if(strstr(serial_msg, "list") == serial_msg){
    byte setting = 0;
    Serial.println("list:");
    while(g_serial_settings[setting].command){
      Serial.print("  "); 
      Serial.print(g_serial_settings[setting].command); 
      Serial.print(": "); 
      Serial.println(g_serial_settings[setting].getter());
      setting++;
    }
    return;
  }
  Serial.print("\""); Serial.print(serial_msg); Serial.println("\" unknown command.\nTry set, get or list.");
}

void parse_serial_parse_input(){
  if(Serial.available() > 0){
    char serial_msg[32] = {0};
    int num = Serial.readBytes(serial_msg, 31);
    if(!num){
      return;
    }
    parse_command(serial_msg);
  }
};
